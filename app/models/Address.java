package models;

import models.utils.AppException;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import java.util.Date;

@Entity
public class Address extends Model {

	@Id 
	//@GeneratedValue(strategy=GenerationType.IDENTITY)
    public Long id;

    @Constraints.Required
    @Formats.NonEmpty
    public String street;
    
    @Constraints.Required
    @Formats.NonEmpty
    public String city;

    @Constraints.Required
    @Formats.NonEmpty
    public String postCode;


    @Constraints.Required
    @Formats.NonEmpty
    public String state;
    
    @Constraints.Required
    @Formats.NonEmpty
    public String country;
    

    // -- Queries (long id, item.class)
    public static Model.Finder<Long, Address> find = new Model.Finder<Long, Address>(Long.class, Address.class);

    /**
     * Retrieve an address from a id.
     *
     * @param addressId Address Id
     * @return an Address
     */
    public static Address findByAddressId(Long id) {
        return find.where().eq("id", id).findUnique();
    }

	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
    
	@Override
	public String toString() {
		return "Address [id=" + id + ", street=" + street + ", city=" + city
				+ ", postCode=" + postCode + ", state=" + state + ", country="
				+ country + "]";
	}

}
