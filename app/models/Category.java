package models;

import models.utils.AppException;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import java.util.Date;
import java.util.List;

@Entity
public class Category extends Model {

    @Id
    public Long id;

    @Constraints.Required
    @Formats.NonEmpty
    public String title;
    
    @Constraints.Required
    @Formats.NonEmpty
    public String description;


    @Constraints.Required
    @Formats.NonEmpty
    public Long parentId;
    
    public String image;

    // -- Queries (long id, item.class)
    public static Model.Finder<Long, Category> find = new Model.Finder<Long, Category>(Long.class, Category.class);

    /**
     * Retrieve an address from a id.
     *
     * @param addressId Address Id
     * @return an Address
     */
    public static Category findByCategoryId(Long id) {
        return find.where().eq("id", id).findUnique();
    }
    
    public static List<Category> findAll() {
        return find.all();
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getImage() {
		return "images/category/"+image;
	}

	public void setImage(String image) {
		this.image = image;
	}
    
    

}
