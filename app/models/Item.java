package models;

import models.utils.AppException;
import models.utils.Hash;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import java.util.Date;

@Entity
public class Item extends Model {

	@Id 
	//@GeneratedValue(strategy=GenerationType.IDENTITY)
    public Long id;
    
    @Constraints.Required
    @Formats.NonEmpty
    public String title;

    @Constraints.Required
    @Formats.NonEmpty
    public String description;

    @Lob
    public byte[] picture;
    
    //@Constraints.Required
    //@Formats.NonEmpty
    public String image;
    

    @ManyToOne(fetch=FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinColumn(name="owner_id", nullable=false)
    public User user;

    @Formats.DateTime(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date dateCreation;

    @Formats.NonEmpty
    public Boolean validated = false;
    
    
    /******************************************/
    @Transient
    public String category;
    @Constraints.Required
    @Formats.NonEmpty
    public String street;
    @Constraints.Required
    @Formats.NonEmpty
    public String city;
    @Constraints.Required
    @Formats.NonEmpty
    public String postCode;
    @Constraints.Required
    @Formats.NonEmpty
    public String state;
    @Constraints.Required
    @Formats.NonEmpty
    public String country;
    
    /*****************************************/

    // -- Queries (long id, item.class)
    public static Model.Finder<Long, Item> find = new Model.Finder<Long, Item>(Long.class, Item.class);

    /**
     * Retrieve a item from a id.
     *
     * @param id Item Id
     * @return a item
     */
    public static Item findByItemId(Long id) {    	
        return find.where().eq("id", id).findUnique();
    }
    
    /**
     * Retrieve a item from title.
     *
     * @param id id to search
     * @return a item
     */
    public static List<Item> findByTitle(String title) {
        return find.where().ilike("title", "%" + title + "%").findList();
    }
    
    public static List<Item> findAll() {
        return find.all();
    }

    
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		
		return "images/items/"+image;
		//return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getDateCreation() {
		DateFormat outputFormatter = new SimpleDateFormat("MM/dd/yyyy");
		return outputFormatter.format(dateCreation);
		//return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Boolean getValidated() {
		return validated;
	}

	public void setValidated(Boolean validated) {
		this.validated = validated;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	
	public byte[] getPicture() {
		return picture;
	}

	public void setPicture(byte[] picture) {
		this.picture = picture;
	}

	@Override
	public String toString() {
		return "Item [id=" + id + ", title=" + title + ", description="
				+ description + ", picture=" + Arrays.toString(picture)
				+ ", image=" + image + ", user=" + user + ", dateCreation="
				+ dateCreation + ", validated=" + validated + ", category="
				+ category + ", street=" + street + ", city=" + city
				+ ", postCode=" + postCode + ", state=" + state + ", country="
				+ country + "]";
	}


    
    
}
