package models;

import models.utils.AppException;
import models.utils.Hash;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.*;
import java.util.Date;

@Entity
public class Message extends Model {

    @Id
    public Long id;

    //@Constraints.Required
    @Formats.NonEmpty
    public Long itemId;

    //@Constraints.Required
    @Formats.NonEmpty
    public Long senderId;
    
    //@Constraints.Required
    @Formats.NonEmpty
    public Long receiverId;
    
    //@Constraints.Required
    @Formats.NonEmpty
    public String messageText;
     

    @Formats.DateTime(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date dateCreation;


    // -- Queries (long id, item.class)
    public static Model.Finder<Long, Message> find = new Model.Finder<Long, Message>(Long.class, Message.class);
    
    /**
     * Retrieve a message from a id.
     *
     * @param id Item Id
     * @return a item
     */
    public static Message findById(Long id) {
        return find.where().eq("id", id).findUnique();
    }
   
    
    public static List<Message> findByItemId(Long id) {
        return find.where().eq("itemId", id).findList();
    }
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public Long getSenderId() {
		return senderId;
	}

	public void setSenderId(Long senderId) {
		this.senderId = senderId;
	}

	public Long getReceiverId() {
		return receiverId;
	}

	public void setReceiverId(Long receiverId) {
		this.receiverId = receiverId;
	}

	public String getMessageText() {
		return messageText;
	}

	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public String getSenderName(long id)
	{
		User user = User.findById(id);
		
		if(user!=null)
		{
			return user.getDisplayName();
		}
		return "User";
	}
	
	@Override
	public String toString() {
		return "Message [id=" + id + ", itemId=" + itemId + ", senderId="
				+ senderId + ", receiverId=" + receiverId + ", messageText="
				+ messageText + ", dateCreation=" + dateCreation + "]";
	}
    
    

}
