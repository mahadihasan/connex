package models;

import models.utils.AppException;
import models.utils.Hash;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import models.Item;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class CategoryItem extends Model {

    @Id
    public Long id;

    @Constraints.Required
    @Formats.NonEmpty
    public Long categoryId;

    @Constraints.Required
    @Formats.NonEmpty
    public Long itemId;

    // -- Queries (long id, item.class)
    public static Model.Finder<Long, CategoryItem> find = new Model.Finder<Long, CategoryItem>(Long.class, CategoryItem.class);

    /**
     * Retrieve a item from a id.
     *
     * @param id Item Id
     * @return a item
     */
    public static CategoryItem findByItemId(Long id) {
        return find.where().eq("id", id).findUnique();
    }
   
    public static List<Item> findItemsByCategoryId(Long id) {
        List<CategoryItem> categoryItem = find.where().ilike("category_id", id.toString()).findList();
        
        List<Item> itemList =  new ArrayList();
        for(CategoryItem categoryItemObject: categoryItem){
        	  Item item = Item.findByItemId(categoryItemObject.itemId);
        	  itemList.add(item);
        	}
        return itemList;
    }
    
}
