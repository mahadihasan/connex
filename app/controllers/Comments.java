package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import models.Item;
import models.Message;
import models.User;
import models.UserOld;
import models.utils.AppException;
import play.Logger;
import play.data.Form;
import play.data.format.Formats;
import play.data.validation.Constraints;
//import play.i18n.Messages;
//import models.Message;
import static play.data.Form.form;
import views.html.message.create;
import views.html.account.additem.created;

public class Comments extends Controller {

	 public static Result create() {
	        return ok(create.render(form(Message.class)));
	    }
	 public static Result save() {
	        Form<Message> messageForm = form(Message.class).bindFromRequest();
	        Message messageObj = new Message();
	        messageObj.itemId = messageForm.get().itemId;
	        messageObj.messageText = messageForm.get().messageText;
	        messageObj.senderId = messageForm.get().senderId;
	        messageObj.receiverId = messageForm.get().receiverId;
	        messageObj.save();
	        System.out.println(messageForm.get().itemId);
	        return redirect(controllers.account.routes.AddItem.showItem(messageForm.get().itemId));
	 }
}
