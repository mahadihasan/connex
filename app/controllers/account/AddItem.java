package controllers.account;

import controllers.Application;
import models.Address;
import models.Category;
import models.Item;
import models.User;
import models.Message;
import models.utils.AppException;
import models.utils.Hash;
import models.utils.Mail;

import org.apache.commons.mail.EmailException;

import play.Configuration;
import play.Logger;
import play.data.Form;
import play.data.validation.Constraints;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import play.mvc.Result;
import views.html.account.additem.DashboardItem;
import views.html.account.additem.confirm;
import views.html.account.additem.create;
import views.html.account.additem.created;
import views.html.account.additem.itemIndex;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static play.data.Form.form;



public class AddItem extends Controller {
	
    /**
     * Display the create form.
     *
     * @return create form
     */
    public static Result create() {
    	List<Category> categoryList = Category.findAll();
        return ok(create.render(form(Item.class),categoryList));
    }
    
    public static Result search()
    {
    	Form<Application.ItemSearch> addItemForm = form(Application.ItemSearch.class).bindFromRequest();
    	if(addItemForm != null)
    	{
    		System.out.println("it is not  null");
    		System.out.println(addItemForm.get().title);
    	}
    	else
    	{
    		System.out.println("***************************** it is null *****");
    		
    	}
    	
    	
    	//List<Item> itemList = Item.findByTitle(addItemForm.get().title);
    	List<Item> itemList = Item.findByTitle(addItemForm.get().title);
    	List<Category> categoryList = Category.findAll();
        return ok(        		
        		 DashboardItem.render(itemList, categoryList,form(Application.ItemSearch.class))
            );
    }
    
    /**
     * Display the create form only (for the index page).
     *
     * @return create form
     */
    public static Result createFormOnly() {
    	List<Category> categoryList = Category.findAll();
        return ok(create.render(form(Item.class),categoryList));
    }

    /**
     * Save the new user.
     *
     * @return Successfull page or created form if bad
     */
    public static Result save() {
        Form<Item> addItemForm = form(Item.class).bindFromRequest();

        if (addItemForm.hasErrors()) {
        	List<Category> categoryList = Category.findAll();
            return badRequest(create.render(addItemForm,categoryList));
        }
        
        

        Item addItem = addItemForm.get();


        try {
        	
        	System.out.println(addItem.toString());
        	
        	
        	/*Address address = new Address();
            address.city = "Munich";
            address.country = "Germany";
            address.postCode = "80939";
            address.state = "Bayern";
            address.street = "Lind. 25";
            address.save();
        	
        	User user = new User();
        	  		
            user.address = address;
            user.confirmationToken = "abcd";
            user.dateCreation = new Date();
            user.dateOfBirth = new Date();
            user.displayName = "New User";
            user.email = "test@tum.de";
            user.firstName = "First";
            user.image = "image";
            user.lastName = "Last";
            user.passwordHash = "123456";
            user.validated = true;
            user.save();*/
            
            User user = User.findById((long)1);
            
            
        	
        	
            Item item = new Item();
            item.title = addItem.title;
            item.description = addItem.description;
            item.image = "";
            item.validated = true;
            
            item.city = addItem.city;
            item.street = addItem.street;
            item.postCode = addItem.postCode;
            item.state = addItem.state;
            item.country = addItem.country;
            item.dateCreation = new Date();
            
            item.user = user;
            //item.picture = addItem.picture;
            
            MultipartFormData body = request().body().asMultipartFormData();
            FilePart picture = body.getFile("pictureFile");
            if (picture != null) {
              String fileName = picture.getFilename();
              String contentType = picture.getContentType(); 
              File file = picture.getFile();
              
              
              file.renameTo(new File("public/images/items", fileName));
              //item.picture = readImageOldWay(file);
              item.setImage(fileName);
            }
            
            System.out.println(item.toString());
            
            item.save();    

            return ok(created.render());
        } catch (Exception e) {
            Logger.error("Signup.save error", e);
            flash("error", Messages.get("error.technical"));
        }
        List<Category> categoryList = Category.findAll();
        return badRequest(create.render(addItemForm,categoryList));
    }
    
    /*
    public static Result search()
    {
    	//Form<Item> itemForm = form(Item.class).bindFromRequest();
    	//System.out.println(itemForm.get().title);
    	Item item = Item.findByItemId((long)1);		
		List<Item> itemList = Item.findAll();
		List<Category> categoryList = Category.findAll();
        return ok(        		
        		 DashboardItem.render(itemList, categoryList)
            );
    }

*/


    public static Result index() {
		Item item = Item.findByItemId((long)1);		
		List<Item> itemList = Item.findAll();
		List<Category> categoryList = Category.findAll();
        return ok(        		
        		 DashboardItem.render(itemList, categoryList,form(Application.ItemSearch.class))
            );
    }
    public static Result showItem(long id)
    {
    	Item item = Item.findByItemId(id);
    	User userObj = User.findById(item.user.id);
    	System.out.println(userObj.displayName);
        List<Message> messageList = Message.findByItemId(id);
        System.out.println(messageList.size());
    	return ok(itemIndex.render(item,userObj,messageList,form(Message.class)));
    }
    
    public static  byte[] readImageOldWay(File file) throws IOException
    {
      //Logger.getLogger(Main.class.getName()).log(Level.INFO, "[Open File] " + file.getAbsolutePath());
      InputStream is = new FileInputStream(file);
      // Get the size of the file
      long length = file.length();
      // You cannot create an array using a long type.
      // It needs to be an int type.
      // Before converting to an int type, check
      // to ensure that file is not larger than Integer.MAX_VALUE.
      if (length > Integer.MAX_VALUE)
      {
        // File is too large
      }
      // Create the byte array to hold the data
      byte[] bytes = new byte[(int) length];
      // Read in the bytes
      int offset = 0;
      int numRead = 0;
      while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0)
      {
        offset += numRead;
      }
      // Ensure all the bytes have been read in
      if (offset < bytes.length)
      {
        throw new IOException("Could not completely read file " + file.getName());
      }
      // Close the input stream and return bytes
      is.close();
      return bytes;
    }
    
   

}
