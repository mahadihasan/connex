package controllers;

import models.UserOld;
import models.Item;
import models.utils.AppException;
import play.Logger;
import play.data.Form;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.index;
import models.Category;
import static play.data.Form.form;

import java.util.List;

import controllers.account.AddItem;

/**
 * Login and Logout.
 * User: yesnault 
 */
public class Application extends Controller {

    public static Result GO_HOME = redirect(
            routes.Application.index()
    );

    public static Result GO_DASHBOARD = redirect(
            routes.Dashboard.index()
    );

    /**
     * Display the login page or dashboard if connected
     *
     * @return login page or dashboard
     */
    public static Result index() {
        // Check that the email matches a confirmed user before we redirect
        String email = ctx().session().get("email");
        if (email != null) {
            UserOld userOld = UserOld.findByEmail(email);
            if (userOld != null && userOld.validated) {
                return GO_DASHBOARD;
            } else {
                Logger.debug("Clearing invalid session credentials");
                session().clear();
            }
        }
        List<Category> categoryList = Category.findAll();
        System.out.println(categoryList.size());
        return ok(index.render(form(Register.class), form(Login.class), categoryList,form(ItemSearch.class)));
    }

    /**
     * Login class used by Login Form.
     */
    public static class Login {

        @Constraints.Required
        public String email;
        @Constraints.Required
        public String password;

        /**
         * Validate the authentication.
         *
         * @return null if validation ok, string with details otherwise
         */
        public String validate() {

            UserOld userOld = null;
            try {
                userOld = UserOld.authenticate(email, password);
            } catch (AppException e) {
                return Messages.get("error.technical");
            }
            if (userOld == null) {
                return Messages.get("invalid.user.or.password");
            } else if (!userOld.validated) {
                return Messages.get("account.not.validated.check.mail");
            }
            return null;
        }

    }

    public static class Register {

        @Constraints.Required
        public String email;

        @Constraints.Required
        public String fullname;

        @Constraints.Required
        public String inputPassword;

        /**
         * Validate the authentication.
         *
         * @return null if validation ok, string with details otherwise
         */
        public String validate() {
            if (isBlank(email)) {
                return "Email is required";
            }

            if (isBlank(fullname)) {
                return "Full name is required";
            }

            if (isBlank(inputPassword)) {
                return "Password is required";
            }

            return null;
        }

        private boolean isBlank(String input) {
            return input == null || input.isEmpty() || input.trim().isEmpty();
        }
    }   
    
 public static class ItemSearch {
    	
        public String title;

        /*
        @Constraints.Required
        public String inputPassword;

    
        public String validate() {
            if (isBlank(email)) {
                return "Email is required";
            }

            if (isBlank(fullname)) {
                return "Full name is required";
            }

            if (isBlank(inputPassword)) {
                return "Password is required";
            }

            return null;
        }

        private boolean isBlank(String input) {
            return input == null || input.isEmpty() || input.trim().isEmpty();
        }
        */
    }
    /**
     * Handle login form submission.
     *
     * @return Dashboard if auth OK or login form if auth KO
     */
    public static Result authenticate() {
        Form<Login> loginForm = form(Login.class).bindFromRequest();

        Form<Register> registerForm = form(Register.class);
        List<Category> categoryList = Category.findAll();
        if (loginForm.hasErrors()) {
            return badRequest(index.render(registerForm, loginForm,categoryList,form(ItemSearch.class)));
        } else {
            session("email", loginForm.get().email);
            return GO_DASHBOARD;
        }
    }

    public static Result getCategory() {
        Form<Category> categoryForm = form(Category.class).bindFromRequest();

       // Form<Register> registerForm = form(Register.class);

       // if (loginForm.hasErrors()) {
       //     return badRequest(index.render(registerForm, loginForm));
      //  } else {
      //      session("email", loginForm.get().email);
            return GO_DASHBOARD;
        //}
    }

    /**
     * Logout and clean the session.
     *
     * @return Index page
     */
    public static Result logout() {
        session().clear();
        flash("success", Messages.get("youve.been.logged.out"));
        return GO_HOME;
    }
    
    /*public static class AddItem {

        @Constraints.Required
        public String title;

        @Constraints.Required
        public String description;

        //@Constraints.Required
        //public String address;
        
        //@Constraints.Required
        //public String image;
        
        @Constraints.Required
        public String street;
        @Constraints.Required
        public String city;
        @Constraints.Required
        public String postCode;
        @Constraints.Required
        public String state;
        @Constraints.Required
        public String country;

        
        public String validate() {
            if (isBlank(title)) {
                return "Title is required";
            }

            if (isBlank(description)) {
                return "Description is required";
            }

            return null;
        }

        private boolean isBlank(String input) {
            return input == null || input.isEmpty() || input.trim().isEmpty();
        }
    }*/

}