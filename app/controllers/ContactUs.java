package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import views.html.contactUs;

public class ContactUs   extends Controller {
	public static Result create() {
        return ok(contactUs.render());
    }
}
