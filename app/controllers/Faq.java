package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import models.User;
import models.UserOld;
import models.utils.AppException;
import play.Logger;
import play.data.Form;
import play.data.format.Formats;
import play.data.validation.Constraints;
import views.html.faq;

public class Faq  extends Controller {
	public static Result create() {
        return ok(faq.render( ));
    }
}
