package models;

import java.util.Date;
import java.util.List;

import models.*;
import models.utils.AppException;
import models.utils.Hash;

import org.junit.*;

import static org.junit.Assert.*;
import play.test.WithApplication;
import static play.test.Helpers.*;

public class ModelsTest extends WithApplication {
    @Before
    public void setUp() {
        start(fakeApplication(inMemoryDatabase()));
    }
    
    /*
     * Test address creation for all use cases
     */
    @Test
    public void testAddressCreation()
    {
        
        Address address = getAddress(1);
        address.save();
    	
        assertNotNull(address);
        assertEquals("Munich", address.city);
        assertEquals("Germany", address.country);
        assertEquals("80939", address.postCode);
    }
    
    /*
     * Tests user creation in the repository
     */
    @Test
    public void testUserCreation() throws AppException
    {
        
        Address address = getAddress(1);
        address.save();
        
        User user = getUser(1,address);
        user.save();
        
        assertNotNull(user);
        assertEquals("test1@tum.de", user.email);
        assertEquals("user1", user.displayName);
        
        
        User user2 = getUser(2,address);
        user2.save();
    	
        assertNotNull(user2);
        assertEquals("test2@tum.de", user2.email);
        assertEquals("user2", user2.displayName);
        
        User userNew=User.findByEmail("test1@tum.de");
        assertNotNull(userNew);
        assertEquals("user1", userNew.displayName);
    }
    
    /*
     * Create a list of user in the repository
     * and tests user search by Email
     */
    @Test
    public void testUserFindByEmail() throws AppException
    {
        
        Address address = getAddress(1);
        address.save();
        
        User user = getUser(1,address);
        user.save();
        
        assertNotNull(user);
        assertEquals("test1@tum.de", user.email);
        assertEquals("user1", user.displayName);
        
        
        User user2 = getUser(2,address);
        user2.save();
    	
        
        User userNew=User.findByEmail("test1@tum.de");
        assertNotNull(userNew);
        assertEquals("user1", userNew.displayName);
    }
    
    /*
     * Tests item creation in the repository
     */
    
    @Test
    public void testItemCreation() throws AppException
    {
        
        Address address = getAddress(1);
        address.save();
        
        User user = getUser(1, address);
        user.save();
        
        Item item = getItem(1, user, address);
        item.save();
    	
        assertNotNull(item);
        assertEquals("Item1", item.title);
    }
    
    /*
     * Create item list in the repository
     * and test item search by item's id
     */
    @Test
    public void testItemFindByItemId() throws AppException
    {
        
        Address address = getAddress(1);
        address.save();
        
        User user = getUser(1, address);
        user.save();
        
        Item item1 = getItem(1, user, address);
        item1.save();
        
        Item item2 = getItem(2, user, address);
        item2.save();
        
        Item item3 = getItem(3, user, address);
        item3.save();
    	
    	
        Item itemNew1 = Item.findByItemId((long) 1);
        assertNotNull(itemNew1);
        assertEquals("Item1", itemNew1.title);
        
    }
    
    /*
     * Create item list in the repository
     * and test item search by item's title
     */
    @Test
    public void testItemFindByTitle() throws AppException
    {
        
        Address address = getAddress(1);
        address.save();
        
        User user = getUser(1, address);
        user.save();
        
        Item item1 = getItem(1, user, address);
        item1.save();
        
        Item item2 = getItem(2, user, address);
        item2.save();
        
        Item item3 = getItem(3, user, address);
        item3.save();
    	
    	
        Item itemNew2 = Item.findByTitle("Item2");
        assertNotNull(itemNew2);
        assertEquals("Item2", itemNew2.title);
        
    }
    
    /*
     * Get item list from the repository
     * and test if list was created properly
     */
    @Test
    public void testItemList() throws AppException
    {
        
        Address address = getAddress(1);
        address.save();
        
        User user = getUser(1, address);
        user.save();
        
        Item item1 = getItem(1, user, address);
        item1.save();
        
        Item item2 = getItem(2, user, address);
        item2.save();
        
        Item item3 = getItem(3, user, address);
        item3.save();
    	
    	List<Item> itemList = Item.findAll();
        
    	
    	assertNotNull(itemList);
    	assertEquals(3, itemList.size());
    	
    	Item itemNew3 = itemList.get(2);
        assertEquals("Item3", itemNew3.title);
        
    }
    /*
     * Return a mock item object
     * 
     * @param index An index for generating user
     * @param Address Address for the user
     * @return a user
     */
    public Item getItem(int index, User user, Address address)
    {
    	Item item = new Item();
        item.title = "Item"+index;
        item.description = "Description"+index;
        item.image = "ItemImage";
        item.validated = true;
        item.user = user;
        item.dateCreation = new Date();
        
        return item;
    }
    
    /*
     * Return a mock address object
     * 
     * @param index An index for generating address
     * @return an address
     */
    public Address getAddress(int index)
    {
    	Address address = new Address();
        address.city = "Munich";
        address.country = "Germany";
        address.postCode = "80939";
        address.state = "Bayern";
        address.street = "Lind. 25";
        
        return address;
    }
    
    /*
     * Return a mock user object
     * 
     * @param index An index for generating user
     * @param Address Address for the user
     * @return a user
     */
    
    public User getUser(int index, Address address) throws AppException
    {
    	User user = new User();
        user.address = address;
        user.confirmationToken = "abcd";
        user.dateCreation = new Date();
        user.dateOfBirth = new Date();
        user.displayName = "user"+index;
        user.email = "test"+index+"@tum.de";
        user.firstName = "First"+index;
        user.image = "image";
        user.lastName = "Last"+index;
        user.passwordHash = Hash.createPassword("fooTest");;
        user.validated = true;
        
        return user;
    }
    
}